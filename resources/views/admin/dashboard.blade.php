@extends('admin.layouts.admin')

@section('title')
Admin Dashboard
@endsection

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header ">
                    <h5 class="card-title">Users Behavior</h5>
                    <p class="card-category">24 Hours performance</p>
                </div>
                <div class="card-body ">
                    <div class="table-responsive">
                        <table class="table">
                          <thead class=" text-primary">
                            <tr><th>
                              Name
                            </th>
                            <th>
                              Country
                            </th>
                            <th>
                              City
                            </th>
                            <th class="text-right">
                              Salary
                            </th>
                          </tr></thead>
                          <tbody>
                            <tr>
                              <td>
                                Dakota Rice
                              </td>
                              <td>
                                Niger
                              </td>
                              <td>
                                Oud-Turnhout
                              </td>
                              <td class="text-right">
                                $36,738
                              </td>
                            </tr>
                            <tr>
                              <td>
                                Minerva Hooper
                              </td>
                              <td>
                                Curaçao
                              </td>
                              <td>
                                Sinaai-Waas
                              </td>
                              <td class="text-right">
                                $23,789
                              </td>
                            </tr>
                            <tr>
                              <td>
                                Sage Rodriguez
                              </td>
                              <td>
                                Netherlands
                              </td>
                              <td>
                                Baileux
                              </td>
                              <td class="text-right">
                                $56,142
                              </td>
                            </tr>
                            <tr>
                              <td>
                                Philip Chaney
                              </td>
                              <td>
                                Korea, South
                              </td>
                              <td>
                                Overland Park
                              </td>
                              <td class="text-right">
                                $38,735
                              </td>
                            </tr>
                            <tr>
                              <td>
                                Doris Greene
                              </td>
                              <td>
                                Malawi
                              </td>
                              <td>
                                Feldkirchen in Kärnten
                              </td>
                              <td class="text-right">
                                $63,542
                              </td>
                            </tr>
                            <tr>
                              <td>
                                Mason Porter
                              </td>
                              <td>
                                Chile
                              </td>
                              <td>
                                Gloucester
                              </td>
                              <td class="text-right">
                                $78,615
                              </td>
                            </tr>
                            <tr>
                              <td>
                                Jon Porter
                              </td>
                              <td>
                                Portugal
                              </td>
                              <td>
                                Gloucester
                              </td>
                              <td class="text-right">
                                $98,615
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer ">
                    <hr>
                    <div class="stats">
                        <i class="fa fa-history"></i> Updated 3 minutes ago
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="card ">
                <div class="card-header ">
                    <h5 class="card-title">Email Statistics</h5>
                    <p class="card-category">Last Campaign Performance</p>
                </div>
                <div class="card-body ">
                    <canvas id="chartEmail"></canvas>
                </div>
                <div class="card-footer ">
                    <div class="legend">
                        <i class="fa fa-circle text-primary"></i> Opened
                        <i class="fa fa-circle text-warning"></i> Read
                        <i class="fa fa-circle text-danger"></i> Deleted
                        <i class="fa fa-circle text-gray"></i> Unopened
                    </div>
                    <hr>
                    <div class="stats">
                        <i class="fa fa-calendar"></i> Number of emails sent
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-title">NASDAQ: AAPL</h5>
                    <p class="card-category">Line Chart with Points</p>
                </div>
                <div class="card-body">
                    <canvas id="speedChart" width="400" height="100"></canvas>
                </div>
                <div class="card-footer">
                    <div class="chart-legend">
                        <i class="fa fa-circle text-info"></i> Tesla Model S
                        <i class="fa fa-circle text-warning"></i> BMW 5 Series
                    </div>
                    <hr />
                    <div class="card-stats">
                        <i class="fa fa-check"></i> Data information certified
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection