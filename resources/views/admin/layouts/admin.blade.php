<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('/css/admin/bootstrap.min.css') }}">

    <!-- Theme Style -->
    <link rel="stylesheet" href="{{ asset('/css/admin/paper-dashboard.min.css') }}">
</head>

<body>
    <div class="wrapper ">
        @include('admin.section.sidebar')
        <div class="main-panel">
            <!-- Navbar -->
            @include('admin.section.navbar')
            <!-- End Navbar -->
            <!-- <div class="panel-header panel-header-lg">
    
      <canvas id="bigDashboardChart"></canvas>
    
    
    </div> -->
            @yield('content')

            @include('admin.section.footer')
        </div>
    </div>
</body>

</html>
