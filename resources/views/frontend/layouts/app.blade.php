

<!DOCTYPE html>
<html lang="en">

<head>
  <title>Agung Reksa Property</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" href="images/logo.png" type="image/x-icon">


  <link href="https://fonts.googleapis.com/css?family=Abril+Fatface:400,400i|Roboto+Mono&display=swap" rel="stylesheet">


  <link rel="stylesheet" href="{{ asset('/css/site/animate.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/site/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/site/jquery.fancybox.min.css') }}">


  <link rel="stylesheet" href="{{ asset('/fonts/ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/fonts/fontawesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/fonts/flaticon/font/flaticon.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/aos.css') }}">
  <link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">

  <!-- Theme Style -->
  <link rel="stylesheet" href="{{ asset('/css/site/style.css') }}">

</head>

<body>

  @include('frontend.section.header')

  @yield('content')

  <a class="widget-whatsapp" title="Hubungi Kami via WhatsApp" rel="nofollow" target="_blank" href="https://api.whatsapp.com/send?phone=6281290998320&text=Saya ingin bertanya sesuatu"><i class="fa fa-whatsapp"></i></a>

  @include('frontend.section.footer')

  <!-- loader -->
  {{-- <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
      <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
      <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
        stroke="#ffc107" /></svg></div> --}}

  <script src="{{ asset('/js/site/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('/js/site/jquery-migrate-3.0.1.min.js') }}"></script>
  <script src="{{ asset('/js/site/popper.min.js') }}"></script>
  <script src="{{ asset('/js/site/bootstrap.min.js') }}"></script>
  <script src="{{ asset('/js/site/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('/js/site/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('/js/site/jquery.fancybox.min.js') }}"></script>
  <script src="{{ asset('/js/site/jquery.stellar.min.js') }}"></script>
  <script src="{{ asset('/js/site/aos.js') }}"></script>
  <script src="{{ asset('js/site/main.js') }}"></script>

  @yield('additional-script')

</body>

</html>