@extends('frontend.layouts.app')

@section('title')

@endsection

@section('content')

<div class="slider-item overlay" data-stellar-background-ratio="0.5"
    style="background-image: url('images/preview-rumah/corner-view.jpg');">
    <div class="container">
      <div class="row slider-text align-items-center justify-content-center text-center">
        <div class="col-lg-12 col-sm-12">
          <h1 class="mb-4" data-aos="fade-up" data-aos-delay="">Detail Produk</h1>
          <p class="custom-breadcrumbs" data-aos="fade-up" data-aos-delay="100"><a href="{{ route('frontend.index') }}">Home</a> <span class="mx-3">/</span> Detail Produk</p>
        </div>
      </div>
    </div>
  </div>

  <div class="section">
    <div class="container">
      <div class="row mb-5">
        <div class="col-12 mb-5">
          <div class="owl-carousel" id="project-img">
            <div>
              <a href="{{ asset('/images/preview-rumah/corner-view.jpg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/preview-rumah/corner-view.jpg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/preview-rumah/top-view.jpg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/preview-rumah/top-view.jpg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/preview-rumah/side-view.jpg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/preview-rumah/side-view.jpg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/preview-rumah/front-view.jpg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/preview-rumah/front-view.jpg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/preview-rumah/develop.jpg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/preview-rumah/develop.jpg') }}" alt="Image" class="img-fluid"></a>
            </div>
          </div>
        </div>
        <div class="col-12">
          <div class="">
            <h3 class="mb-4">Project Terbaru Kami</h3>
            <p>Rumah Eksklusif dengan harga bersahabat. Paling murah dibandingkan perumahan lain disekitarnya. Yuk survey lokasi agar dapat segera booking. Berikut fasilitas yang dimiliki oleh produk kami: </p>
            
            <div class="row">
              <div class="col-md-6">
                <ul class="list-unstyled mb-5">
                  <li class="mb-3">
                    <strong class="d-block text-black">Fasilitas Utama:</strong>
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Luas Tanah <strong>81 meter</strong>
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Luas Bangunan <strong>45 meter</strong>
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Rangka baja ringan
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Besi selup atas bawah
                  </li>
                  <li class="mb-2">
                  <i class="fa fa-check text-success"></i>   Genteng beton
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Lampu downlight philip
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Carport <strong>4 x 3.5 meter</strong>
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Cat exterior/interior dulux
                  </li>
                  <li class="mb-2">
              <i class="fa fa-check text-success"></i>       Pompa Air
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Listrik <strong>1300 watt</strong>
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> <strong>Surat AJB</strong>
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Taman <strong>(Luar dan Dalam)</strong>
                  </li>
                  <li class="mb-2">
             <i class="fa fa-check text-success"></i>        Mini Bar
                  </li>
                  <li class="mb-2">
                <i class="fa fa-check text-success"></i>     Kitchen Set
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Exhaus fan kompor
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Exhaus fan kamar mandi
                  </li>
                  <li class="mb-2">
           <i class="fa fa-check text-success"></i>          Shower
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Closet duduk (Toto)
                  </li>
                </ul>
              </div>
              
              <div class="col-md-6">
                <ul class="list-unstyled mb-5">
                  <li class="mb-3">
                    <strong class="d-block text-black">Fasilitas dan Akses Penunjang:</strong>
                  </li>
                  <li class="mb-2">
                <i class="fa fa-check text-success"></i>     Jalan Aspal
                  </li>
                  <li class="mb-2">
                <i class="fa fa-check text-success"></i>     Saluran Air
                  </li>
                  <li class="mb-2">
               <i class="fa fa-check text-success"></i>      JungleFest
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> The Jungle Waterpark
                  </li>
                  <li class="mb-2">
              <i class="fa fa-check text-success"></i>       Junglebon
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Rumah Sakit Melania
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Polsek Bogor Selatan
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Hotel Pajajaran Suite
                  </li>
                  <li class="mb-2">
                    <i class="fa fa-check text-success"></i> Akses Jalan Tol hanya 15 menit
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-12 mb-4">
          <h2 class="text-center mb-5">Lokasi</h2>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.0287852500214!2d106.78579891477132!3d-6.643347395196279!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69cf6eb21f1ca7%3A0xcfba45c59b8b652b!2sGg.%20Jempol%202%20No.4%2C%20RT.02%2FRW.04%2C%20Mulyaharja%2C%20Kec.%20Bogor%20Sel.%2C%20Kota%20Bogor%2C%20Jawa%20Barat%2016135!5e0!3m2!1sid!2sid!4v1581145476502!5m2!1sid!2sid" width="100%" height="550" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
      </div>
      
    </div>
  </div>

    {{-- @include('frontend.section.review') --}}
  
@endsection

@section('additional-script')
  <script>
    $(document).ready(function(){
      $("#project-img").owlCarousel({
        items: 1,
        autoplay: true,
        loop: true,
        nav: false,
        margin: 10,
        stagePadding: 50,
        autoplayTimeout: 3000
      });
    });
  </script>
@endsection