@extends('frontend.layouts.app')

@section('title')

@endsection

@section('content')

  <div class="slider-item overlay" data-stellar-background-ratio="0.5" style="background-image: url('images/preview-rumah/corner-view.jpg');">
    <div class="container">
      <div class="row slider-text align-items-center justify-content-center">
        <div class="col-lg-12 text-center col-sm-12">
          <h1 class="mb-4" data-aos="fade-up" data-aos-delay="100">Selamat Datang <br> Di Website Kami</h1>
        </div>
      </div>
    </div>
  </div>

  <div class="section">
    <div class="container">
      <h2 class="text-center mb-5">Keunggulan Kami</h2>

      <div class="row">
        <div class="col-lg-3 mb-4">
          <div class="service text-center" data-aos="fade-up" data-aos-delay="">
              <span class="icon icon-location-pin mb-4 d-block"></span>
              <h3>Lokasi Strategis</h3>
          </div>
        </div>
        <div class="col-lg-3 mb-4">
          <div class="service text-center" data-aos="fade-up" data-aos-delay="100">
              <span class="icon icon-badge mb-4 d-block"></span>
              <h3>Hunian Nyaman</h3>
          </div>
        </div>
        <div class="col-lg-3 mb-4">
          <div class="service text-center" data-aos="fade-up" data-aos-delay="200">
              <span class="icon icon-feed mb-4 d-block"></span>
              <h3>Free Wifi</h3>
          </div>
        </div>
        <div class="col-lg-3 mb-4">
          <div class="service text-center" data-aos="fade-up" data-aos-delay="300">
              <span class="icon icon-check mb-4 d-block"></span>
              <h3>Fasilitas Terjamin</h3>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="section">
    <div class="container">
      <div class="row">
        <div class="col-12 mb-5">
          <h2 class="text-center mb-4">Progress Pembangunan Rumah</h2>
          <div class="owl-carousel" id="project-img">
            <div>
              <a href="{{ asset('/images/progress/progress-1.jpeg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/progress/progress-1.jpeg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/progress/progress-2.jpeg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/progress/progress-2.jpeg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/progress/progress-3.jpeg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/progress/progress-3.jpeg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/progress/progress-4.jpeg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/progress/progress-4.jpeg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/progress/progress-5.jpeg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/progress/progress-5.jpeg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/progress/progress-6.jpeg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/progress/progress-6.jpeg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/progress/progress-7.jpeg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/progress/progress-7.jpeg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/progress/progress-9.jpeg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/progress/progress-9.jpeg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/progress/progress-10.jpg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/progress/progress-9.jpeg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/progress/progress-11.jpg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/progress/progress-9.jpeg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/progress/progress-12.jpg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/progress/progress-9.jpeg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/progress/progress-13.jpg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/progress/progress-9.jpeg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/progress/progress-14.jpg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/progress/progress-9.jpeg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/progress/progress-15.jpeg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/progress/progress-9.jpeg') }}" alt="Image" class="img-fluid"></a>
            </div>
            <div>
              <a href="{{ asset('/images/progress/progress-16.jpeg') }}" class="d-block" data-fancybox="gal"><img src="{{ asset('/images/progress/progress-9.jpeg') }}" alt="Image" class="img-fluid"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="section portfolio-section">
    <div class="container">
      <div class="row mb-5 justify-content-center" data-aos="fade-up">
        <div class="col-md-8 text-center">
          <h2 class="mb-4">Lihat Produk Terbaru Kami</h2>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row mb-5 no-gutters">
        <div class="col-sm-6 col-md-12 col-lg-12" data-aos="fade" data-aos-delay="100">
          <a href="{{ route('frontend.detail-project') }}" class="work-thumb">
            <div class="work-text">
              <h2>Lihat detail project</h2>
            </div>
            <img src="images/preview-rumah/front-view.jpg" alt="Image" class="img-fluid">
          </a>
        </div>

        {{-- <div class="col-sm-6 col-md-6 col-lg-6" data-aos="fade" data-aos-delay="200">
          <a href="portfolio-single.html" class="work-thumb">
            <div class="work-text">
              <h2>Agung Reksa Property</h2>
              <p>Design</p>
            </div>
            <img src="images/preview-rumah/corner-view.jpg" alt="Image" class="img-fluid">
          </a>
        </div>

        <div class="col-sm-6 col-md-6 col-lg-6" data-aos="fade" data-aos-delay="300">
          <a href="portfolio-single.html" class="work-thumb">
            <div class="work-text">
              <h2>Agung Reksa Property</h2>
              <p>Business</p>
            </div>
            <img src="images/preview-rumah/top-view.jpg" alt="Image" class="img-fluid">
          </a>
        </div>

        <div class="col-sm-6 col-md-6 col-lg-6" data-aos="fade" data-aos-delay="400">
          <a href="portfolio-single.html" class="work-thumb">
            <div class="work-text">
              <h2>Agung Reksa Property</h2>
              <p>Business</p>
            </div>
            <img src="images/preview-rumah/side-view.jpg" alt="Image" class="img-fluid">
          </a>
        </div> --}}

      </div>

    </div>
  </div>

  {{-- @include('frontend.section.review') --}}

  <div class="section">
    <div class="container">
      <div class="row">

        <div class="col-lg-12">

          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.0287852500214!2d106.78579891477132!3d-6.643347395196279!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69cf6eb21f1ca7%3A0xcfba45c59b8b652b!2sGg.%20Jempol%202%20No.4%2C%20RT.02%2FRW.04%2C%20Mulyaharja%2C%20Kec.%20Bogor%20Sel.%2C%20Kota%20Bogor%2C%20Jawa%20Barat%2016135!5e0!3m2!1sid!2sid!4v1581145476502!5m2!1sid!2sid" width="100%" height="550" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

        </div>
      </div>
    </div>
  </div>

  <div class="bg-primary py-5">
    <div class="container text-center">
      <div class="row justify-content-center">
        <div class="col-lg-7">
          <h3 class="text-white mb-2" >Get Started</h3>
          <p class="text-white mb-4">Berikan pesan dan kesan anda terhadap produk kami.</p>

          <p class="mb-0" data-aos="fade-up" data-aos-delay="200"><a href="{{ route('frontend.contact') }}" class="btn btn-outline-white px-4 py-3">Get In Touch!</a></p>
        </div>
      </div>

    </div>
  </div>
  
@endsection

@section('additional-script')
  <script>
    $(document).ready(function(){
      $("#project-img").owlCarousel({
        items: 1,
        autoplay: true,
        loop: true,
        nav: false,
        margin: 10,
        stagePadding: 50,
        autoplayTimeout: 3000
      });
    });
  </script>
@endsection