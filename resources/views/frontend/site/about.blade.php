@extends('frontend.layouts.app')

@section('title')

@endsection

@section('content')

<div class="slider-item overlay" data-stellar-background-ratio="0.5"
    style="background-image: url('images/preview-rumah/corner-view.jpg');">
    <div class="container">
      <div class="row slider-text align-items-center justify-content-center text-center">
        <div class="col-lg-12 col-sm-12">
          <h1 class="mb-4" data-aos="fade-up" data-aos-delay="">Tentang Kami</h1>
          <p class="custom-breadcrumbs" data-aos="fade-up" data-aos-delay="100"><a href="{{ route('frontend.index') }}">Home</a> <span class="mx-3">/</span> About</p>
        </div>
      </div>
    </div>
  </div>

  <div class="section">
    <div class="container">
      
      <div class="row">
        <div class="col-12 mb-4">
          <h2 class="text-center mb-5">Agung Reksa Dipa Property</h2>
            <p class="text-center">Menawarkan rumah siap huni dengan berbagai type dan dengan Konsep hunian aman, nyaman, nan elegan dengan  spesifikasi material terbaik. Berdiri di area ketinggian dengan desain rumah mewah menawan, ekslusif dengan pemandangan gunung salak dan tidak jauh dari tempat rekreasi Bogor Nirwana Residence yang pasti akan menjadi hunian yang ideal di dalam hidup Anda dan keluarga.</p>
        </div>
      </div>
      
      <div class="row">
        <div class="col-12 mb-4">
          <h2 class="text-center mb-5">Kunjungi Kami</h2>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.0287852500214!2d106.78579891477132!3d-6.643347395196279!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69cf6eb21f1ca7%3A0xcfba45c59b8b652b!2sGg.%20Jempol%202%20No.4%2C%20RT.02%2FRW.04%2C%20Mulyaharja%2C%20Kec.%20Bogor%20Sel.%2C%20Kota%20Bogor%2C%20Jawa%20Barat%2016135!5e0!3m2!1sid!2sid!4v1581145476502!5m2!1sid!2sid" width="100%" height="550" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
      </div>

    </div>
  </div>

  {{-- <div class="section">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6 order-md-2" data-aos="fade-up" data-aos-delay="100">
          <figure class="img-dotted-bg">
            <img src="images/preview-rumah/lokasi.jpg" alt="Image" class="img-fluid">
          </figure>
        </div>
        <div class="col-md-5 mr-auto" data-aos="fade-up" data-aos-delay="">
          <h2 class="mb-4 section-title">Lokasi</h2>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, quos, adipisci aliquid similique
            saepe ipsa minus maxime alias libero nam quis officia eum impedit. At quisquam reprehenderit cum hic enim?</p>
          <p>Necessitatibus eligendi molestias similique tempore, optio nobis numquam temporibus debitis cum aspernatur,
            eius, nihil soluta sapiente enim. </p>
        </div>
      </div>
    </div>
  </div>
  
  <div class="section">
    <div class="container">
      <div class="row justify-content-center mb-5">
        <div class="col-md-8 text-center" data-aos="fade-up" data-aos-delay="">
          <h2 class="mb-4 section-title">Tipe Produk</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4" data-aos="fade-up" data-aos-delay="100">
          <div class="media d-block media-custom text-center">
            <a href="#"><img src="images/preview-rumah/side-view.jpg" alt="Image Placeholder" class="img-fluid"></a>
            <div class="media-body">
              <h3 class="mt-0 text-black">Agung Reksa</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam minus repudiandae amet.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4" data-aos="fade-up" data-aos-delay="200">
          <div class="media d-block media-custom text-center">
            <a href="#"><img src="images/preview-rumah/front-view.jpg" alt="Image Placeholder" class="img-fluid"></a>
            <div class="media-body">
              <h3 class="mt-0 text-black">Agung Reksa</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam minus repudiandae amet.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4" data-aos="fade-up" data-aos-delay="300">
          <div class="media d-block media-custom text-center">
            <a href="#"><img src="images/preview-rumah/corner-view.jpg" alt="Image Placeholder" class="img-fluid"></a>
            <div class="media-body">
              <h3 class="mt-0 text-black">Agung Reksa</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam minus repudiandae amet.</p>
            </div>
          </div>
        </div>
  
      </div>
  
    </div>
  </div>
  <!-- END section -->

  <div class="section">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6" data-aos="fade-up" data-aos-delay="100">
          <figure class="img-dotted-bg">
            <img src="images/preview-rumah/top-view.jpg" alt="Image" class="img-fluid">
          </figure>
        </div>
        <div class="col-md-5 ml-auto" data-aos="fade-up" data-aos-delay="">
          <h2 class="mb-4 section-title">Detail Produk</h2>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, quos, adipisci aliquid similique
            saepe ipsa minus maxime alias libero nam quis officia eum impedit. At quisquam reprehenderit cum hic enim?</p>
          <p>Necessitatibus eligendi molestias similique tempore, optio nobis numquam temporibus debitis cum aspernatur,
            eius, nihil soluta sapiente enim. </p>
        </div>
      </div>
    </div>
  </div> --}}

  {{-- @include('frontend.section.review') --}}
  
@endsection