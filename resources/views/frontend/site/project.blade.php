@extends('frontend.layouts.app')

@section('title')

@endsection

@section('content')

<div class="slider-item overlay" data-stellar-background-ratio="0.5"
style="background-image: url('images/preview-rumah/corner-view.jpg');">
<div class="container">
  <div class="row slider-text align-items-center justify-content-center text-center">
    <div class="col-lg-12 col-sm-12">
      <h1 class="mb-4" data-aos="fade-up" data-aos-delay="">Our Projects</h1>
      <p class="custom-breadcrumbs" data-aos="fade-up" data-aos-delay="100"><a href="{{ route('frontend.index') }}">Home</a> <span class="mx-3">/</span> Projects</p>
    </div>
  </div>
</div>
</div>

<div class="section portfolio-section">
    <div class="container">
    <div class="row mb-5 justify-content-center" data-aos="fade-up">
        <div class="col-md-8 text-center">
        <h2 class="mb-4 section-title">Project Terbaru Kami</h2>
        <p>Berikut ini adalah preview project terbaru yang sedang kami kerjakan.</p>
        </div>
    </div>
    </div>
    <div class="container">
    <div class="row mb-5 no-gutters">
        <div class="col-sm-6 col-md-6 col-lg-6" data-aos="fade" data-aos-delay="100">
        <a href="{{ route('frontend.detail-project') }}" class="work-thumb">
            <div class="work-text">
            <h2>Lihat detail project</h2>
            </div>
            <img src="images/preview-rumah/top-view.jpg" alt="Image" class="img-fluid">
        </a>
        </div>

        <div class="col-sm-6 col-md-6 col-lg-6" data-aos="fade" data-aos-delay="200">
        <a href="{{ route('frontend.detail-project') }}" class="work-thumb">
            <div class="work-text">
            <h2>Lihat detail project</h2>
            </div>
            <img src="images/preview-rumah/side-view.jpg" alt="Image" class="img-fluid">
        </a>
        </div>

        <div class="col-sm-6 col-md-6 col-lg-6" data-aos="fade" data-aos-delay="300">
        <a href="{{ route('frontend.detail-project') }}" class="work-thumb">
            <div class="work-text">
            <h2>Lihat detail project</h2>
            </div>
            <img src="images/preview-rumah/front-view.jpg" alt="Image" class="img-fluid">
        </a>
        </div>

        <div class="col-sm-6 col-md-6 col-lg-6" data-aos="fade" data-aos-delay="400">
        <a href="{{ route('frontend.detail-project') }}" class="work-thumb">
            <div class="work-text">
            <h2>Lihat detail project</h2>
            </div>
            <img src="images/preview-rumah/corner-view.jpg" alt="Image" class="img-fluid">
        </a>
        </div>

        <div class="col-sm-6 col-md-6 col-lg-6" data-aos="fade" data-aos-delay="100">
        <a href="{{ route('frontend.detail-project') }}" class="work-thumb">
            <div class="work-text">
            <h2>Lihat detail project</h2>
            </div>
            <img src="images/preview-rumah/top-view.jpg" alt="Image" class="img-fluid">
        </a>
        </div>

        <div class="col-sm-6 col-md-6 col-lg-6" data-aos="fade" data-aos-delay="200">
        <a href="{{ route('frontend.detail-project') }}" class="work-thumb">
            <div class="work-text">
            <h2>Lihat detail project</h2>
            </div>
            <img src="images/preview-rumah/side-view.jpg" alt="Image" class="img-fluid">
        </a>
        </div>

        <div class="col-sm-6 col-md-6 col-lg-6" data-aos="fade" data-aos-delay="300">
        <a href="{{ route('frontend.detail-project') }}" class="work-thumb">
            <div class="work-text">
            <h2>Lihat detail project</h2>
            </div>
            <img src="images/preview-rumah/front-view.jpg" alt="Image" class="img-fluid">
        </a>
        </div>

        <div class="col-sm-6 col-md-6 col-lg-6" data-aos="fade" data-aos-delay="400">
        <a href="{{ route('frontend.detail-project') }}" class="work-thumb">
            <div class="work-text">
            <h2>Lihat detail project</h2>
            </div>
            <img src="images/preview-rumah/corner-view.jpg" alt="Image" class="img-fluid">
        </a>
        </div>

    </div>
    
    </div>
    </div>

    {{-- @include('frontend.section.review') --}}
  
@endsection