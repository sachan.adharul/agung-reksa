@extends('frontend.layouts.app')

@section('title')

@endsection

@section('content')

<div class="slider-item overlay" data-stellar-background-ratio="0.5"
    style="background-image: url('images/preview-rumah/corner-view.jpg');">
    <div class="container">
      <div class="row slider-text align-items-center justify-content-center text-center">
        <div class="col-lg-12 col-sm-12">
          <h1 class="mb-4" data-aos="fade-up" data-aos-delay="">Our Services</h1>
          <p class="custom-breadcrumbs" data-aos="fade-up" data-aos-delay="100"><a href="{{ route('frontend.index') }}">Home</a> <span class="mx-3">/</span> Services</p>
        </div>
      </div>
    </div>
  </div>

  <div class="section">
    <div class="container">
      <div class="row">
        
        <div class="col-lg-4 col-md-6 mb-5 text-center"  data-aos="fade-up" data-aos-delay="">
          <div class="service">
            <span class="icon icon-cursor mb-4 d-block"></span>
            <h3>Branding</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing onsectetur adipisicing elit.</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 mb-5 text-center"  data-aos="fade-up" data-aos-delay="100">
          <div class="service">
            <span class="icon icon-badge mb-4 d-block"></span>
            <h3>Web Design</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing onsectetur adipisicing elit.</p>
          </div>
        </div>
    
        <div class="col-lg-4 col-md-6 mb-5 text-center"  data-aos="fade-up" data-aos-delay="200">
          <div class="service">
            <span class="icon icon-fire mb-4 d-block"></span>
            <h3>App Design</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing onsectetur adipisicing elit.</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 mb-5 text-center"  data-aos="fade-up" data-aos-delay="">
          <div class="service">
            <span class="icon icon-layers mb-4 d-block"></span>
            <h3>Start Up</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing onsectetur adipisicing elit.</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 mb-5 text-center"  data-aos="fade-up" data-aos-delay="100">
          <div class="service">
            <span class="icon icon-mouse mb-4 d-block"></span>
            <h3>Branding</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing onsectetur adipisicing elit.</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 mb-5 text-center"  data-aos="fade-up" data-aos-delay="200">
          <div class="service">
            <span class="icon icon-magnet mb-4 d-block"></span>
            <h3>Creative Design</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing onsectetur adipisicing elit.</p>
          </div>
        </div>
        
        <div class="col-lg-4 col-md-6 mb-5 text-center"  data-aos="fade-up" data-aos-delay="0">
          <div class="service">
            <span class="icon icon-paper-plane mb-4 d-block"></span>
            <h3>Email Marketing</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing onsectetur adipisicing elit.</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 mb-5 text-center"  data-aos="fade-up" data-aos-delay="100">
          <div class="service">
            <span class="icon icon-handbag mb-4 d-block"></span>
            <h3>Shopping Cart</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing onsectetur adipisicing elit.</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 mb-5 text-center"  data-aos="fade-up" data-aos-delay="200">
          <div class="service">
            <span class="icon icon-rocket mb-4 d-block"></span>
            <h3>Start Up</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing onsectetur adipisicing elit.</p>
          </div>
        </div>
          
      </div>
    </div>
  </div>

  {{-- @include('frontend.section.review') --}}
  
@endsection