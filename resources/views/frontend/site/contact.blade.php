@extends('frontend.layouts.app')

@section('title')

@endsection

@section('content')

<div class="slider-item overlay" data-stellar-background-ratio="0.5"
    style="background-image: url('images/preview-rumah/corner-view.jpg');">
    <div class="container">
      <div class="row slider-text align-items-center justify-content-center text-center">
        <div class="col-lg-12 col-sm-12">
          <h1 class="mb-4" data-aos="fade-up" data-aos-delay="">Contact Us</h1>
          <p class="custom-breadcrumbs" data-aos="fade-up" data-aos-delay="100"><a href="index.html">Home</a> <span class="mx-3">/</span> Contact</p>
        </div>
      </div>
    </div>
  </div>

  <div class="section">
    <div class="container">
      <div class="row mb-5">
        <div class="col-12 contact-form-contact-info">
          <div class="row">
            <div class="col-lg-4" data-aos="fade-up" data-aos-delay="">
              <p class="d-flex">
                <span class="ion-ios-location icon mr-5"></span>
                <span>Gg. Jempol 2 4, RW.04, Mulyaharja, Kec. Bogor Selatan, Kota Bogor, Jawa Barat. Kode Pos 16135</span>
              </p>
            </div>
            <div class="col-lg-4" data-aos="fade-up" data-aos-delay="100">
              <p class="d-flex">
                <span class="ion-ios-telephone icon mr-5"></span>
                <span>+62 812 9099 8320</span>
              </p>
            </div>
            <div class="col-lg-4" data-aos="fade-up" data-aos-delay="200">
              <p class="d-flex">
                <span class="ion-android-mail icon mr-5"></span>
                <span>agungreksadipa@gmail.com</span>
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="row mt-5">

        <div class="col-12 mb-5 order-2">
          <form action="#" method="post">
            <div class="row">
              <div class="col-md-6 form-group">
                <label for="name">Nama</label>
                <input type="text" id="name" class="form-control ">
              </div>
              <div class="col-md-6 form-group">
                <label for="phone">Nomor Handphone</label>
                <input type="text" id="phone" class="form-control ">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
  
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <label for="email">Email</label>
                <input type="email" id="email" class="form-control ">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <label for="message">Tinggalkan Pesan</label>
                <textarea name="message" id="message" class="form-control " cols="30" rows="8"></textarea>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 form-group">
                <input type="submit" value="Kirim" class="btn btn-outline-black px-3 py-3">
              </div>
            </div>
          </form>
        </div>

        
      </div>

      
    </div>
  </div>

  {{-- @include('frontend.section.review') --}}
  
@endsection