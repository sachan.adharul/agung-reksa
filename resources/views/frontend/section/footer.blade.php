<footer class="site-footer" role="contentinfo">
    <div class="container">
      <div class="row mb-5">
        <div class="col-md-4 mb-5">
          <h3 class="mb-4">Tentang Kami</h3>
          <p class="mb-5">Menawarkan rumah siap huni dengan berbagai type dan dengan Konsep hunian aman, nyaman, nan elegan dengan  spesifikasi material terbaik.</p>


        </div>
        <div class="col-md-3 ml-auto">

          <h3 class="mb-4">Navigasi</h3>
          <ul class="list-unstyled footer-link">
            <li><a href="{{ route('frontend.about') }}">Tentang Kami</a></li>
            {{-- <li><a href="{{ route('frontend.services') }}">Services</a></li> --}}
            <li><a href="{{ route('frontend.projects') }}">Portfolio</a></li>
            <li><a href="{{ route('frontend.contact') }}">Kontak</a></li>
          </ul>

        </div>
        <div class="col-md-3 mb-5">
          <ul class="list-unstyled footer-link d-flex footer-social">
            <li><a href="https://twitter.com/emanjaro728" target="_blank" class="p-2"><span class="fa fa-twitter"></span></a></li>
            <li><a href="https://web.facebook.com/eman.jaro.3" target="_blank" class="p-2"><span class="fa fa-facebook"></span></a></li>
            <li><a href="#" target="_blank" class="p-2"><span class="fa fa-instagram"></span></a></li>
          </ul>
        </div>

      </div>
    </div>
</footer>
<!-- END footer -->