<header role="banner">
    <nav class="navbar navbar-expand-lg bg-light">
      <div class="container">
        <a class="navbar-brand " href="{{ route('frontend.index') }}">
          <img src="{{ asset('/images/logo.png') }}" alt="Agung Reksa Property">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05"
          aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>


        <div class="collapse navbar-collapse" id="navbarsExample05">
          <ul class="navbar-nav pl-md-5 ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('frontend.index') }}">Beranda</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('frontend.projects') }}">Projects</a>
            </li>
            {{-- <li class="nav-item dropdown">
              <a class="nav-link" href="{{ route('frontend.services') }}">Layanan</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="{{ route('frontend.articles') }}">Artikel</a>
            </li> --}}
            <li class="nav-item">
              <a class="nav-link" href="{{ route('frontend.about') }}">Tentang Kami</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('frontend.contact') }}">Kontak</a>
            </li>
          </ul>


        </div>
      </div>
    </nav>
</header>
<!-- END header -->