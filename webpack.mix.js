const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/site/main.js', 'public/js/site')
    .js('resources/js/admin/paper-dashboard.js', 'public/js/admin')

    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/site/style.scss', 'public/css/site')
    .sass('resources/sass/admin/paper-dashboard.scss', 'public/css/admin')
    
    .browserSync({
        proxy: 'agung-reksa'
    });
