<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
   {
      return view('frontend.site.index');
   }
   
   public function projects()
   {
      return view('frontend.site.project');
   }
   
   public function projectDetail()
   {
      return view('frontend.site.projects.detail');
   }
   
   public function services()
   {
      return view('frontend.site.service');
   }
   
   public function articles()
   {
      return view('frontend.site.article');
   }
   
   public function about()
   {
      return view('frontend.site.about');
   }
   
   public function contact()
   {
      return view('frontend.site.contact');
   }
}
